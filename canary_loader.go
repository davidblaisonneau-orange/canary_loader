package main

/* This program load web server and test canary release as described in
https://particule.io/blog/argocd-canary/. It shows proportion of server
answers, to illustrate canary deployment. */
import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"sort"
	"strings"
	"sync"
	"time"

	"golang.org/x/term"
)

// Json schema sent by the server
type HTTPResponse struct {
	Status string `json:"status"`
	Color  string `json:"color"`
}

// Call url api on ticker trigger and send answer to result channel.
func get(
	url string, // url to call
	ticker *time.Ticker, // ticker that trigger the GET on url
	done chan bool, // chan to stop the routine
	results chan<- HTTPResponse) { // results channel

	for {
		select {
		// stop when 'done' channel is True
		case <-done:
			return
		case <-ticker.C:
			// Create a HTTP Client and call URL
			client := http.Client{
				Timeout: time.Second * 2,
			}
			req, err := http.NewRequest(http.MethodGet, url, nil)
			if err != nil {
				panic(err)
			}
			req.Header.Set("User-Agent", "canary-client")

			res, getErr := client.Do(req)
			if getErr != nil {
				log.Fatal(getErr)
			}

			// Read body and parse answer
			if res.Body != nil {
				defer res.Body.Close()
			}

			body, readErr := ioutil.ReadAll(res.Body)
			if readErr != nil {
				log.Fatal(readErr)
			}

			http_response1 := HTTPResponse{}
			jsonErr := json.Unmarshal(body, &http_response1)
			if jsonErr != nil {
				log.Fatal(jsonErr)
			}
			// Transfer to results channel
			results <- http_response1
		}
	}
}

// Store results to an array that will contains last second results
func storeRes(
	results <-chan HTTPResponse, // result channel
	done <-chan bool, // chan to stop the routine
	reqBySec int, // number of requests by second
	numJobs int, // quantity of parallel jobs
	endTime time.Time) { // Date of end of the app

	last_stats := []HTTPResponse{}
	var m sync.Mutex

	// Start Go routine that will print results statistics
	go printStats(
		&last_stats,
		time.NewTicker(time.Duration(200*time.Millisecond)),
		done,
		&m,
		reqBySec,
		endTime)
	for {
		// Get results
		res, more := <-results
		if more {
			// Mutex lock to ensure result list will not be read at write
			m.Lock()
			// Add result to list
			last_stats = append(last_stats, res)
			// Limit result list to 1 second
			if len(last_stats) > reqBySec {
				last_stats = last_stats[1:reqBySec]
			}
			// Unlock mutex
			m.Unlock()
		} else {
			return
		}
	}
}

// Print console statistics
func printStats(
	last_stats *[]HTTPResponse, // List of last second responses
	ticker *time.Ticker, // Ticker for status refresh
	done <-chan bool, // chan to stop the routine
	m *sync.Mutex, // Mutex to avoid read on changing data.
	reqBySec int, // Number of requests by seconds (for display)
	endTime time.Time) { // Date of end of the app

	// Get CLI Window size
	termWidth, _, _ := term.GetSize(int(os.Stdin.Fd()))
	termWidth -= 15

	// Store last second values
	history := []int{0, 0, 0, 0, 0}
	for {
		select {
		case <-done:
			fmt.Println()
			return
		case <-ticker.C:
			if len(*last_stats) > 0 {
				// Read results and map them
				m.Lock()
				status := make(map[string]int)
				colors := make(map[string]int)
				for i := 0; i < len(*last_stats); i++ {
					status[(*last_stats)[i].Status]++
					colors[(*last_stats)[i].Color]++
				}
				*last_stats = nil
				m.Unlock()

				// Get status and colors keys to enforce sorted display
				s_keys := make([]string, 0, len(status))
				s_sum := 0
				for key := range status {
					s_sum += status[key]
					s_keys = append(s_keys, key)
				}
				// Store number of status in history
				history = append(history, s_sum)
				history = history[1:6]
				// sort keys to ensure coheren printing
				sort.Strings(s_keys)
				// sum of status (for formating purpose)
				s_sum = int(float64(s_sum) * 100 / float64(termWidth))

				c_keys := make([]string, 0, len(colors))
				c_sum := 0
				for key := range colors {
					c_sum += colors[key]
					c_keys = append(c_keys, key)
				}
				sort.Strings(c_keys)
				// sum of colors (for formating purpose)
				c_sum = int(float64(c_sum) * 100 / float64(termWidth))

				// Clear screen
				fmt.Print("\033[2J\033[H")

				// Display headers
				fmt.Println("Canary demo")
				fmt.Println("===========")
				effectiveReqBySec := 0
				for _, v := range history {
					effectiveReqBySec += v
				}
				fmt.Println("Requests by seconds (real / target):",
					effectiveReqBySec, "/", reqBySec)
				fmt.Println("Ends in:", endTime.Sub(time.Now().Local()))

				// Print status repartition
				fmt.Println("Status repartition:", s_keys)
				fmt.Print("Percent: ")
				i := 0
				for _, k := range s_keys {
					status[k] = int(math.Round(
						float64(status[k] * 100 / s_sum)))
					if i%2 == 0 {
						fmt.Print(strings.Repeat("#", status[k]))
					} else {
						fmt.Print(strings.Repeat("_", status[k]))
					}
					i += 1
				}
				fmt.Println()

				// Print color repartition
				fmt.Println("Colors repartition: ", c_keys)
				fmt.Print("Percent: ")
				i = 0
				for _, k := range c_keys {
					colors[k] = int(math.Round(
						float64(colors[k] * 100 / c_sum)))
					if i%2 == 0 {
						fmt.Print(strings.Repeat("#", colors[k]))
					} else {
						fmt.Print(strings.Repeat("_", colors[k]))
					}
					i += 1
				}
			}

		}
	}
}

func main() {
	// Get parameters from CLI
	serverUrl := flag.String("url", "http://127.0.0.1:8090", "API url")
	reqBySec := flag.Int("req", 1000, "requests by seconds")
	numJobs := flag.Int("p", 5, "parallele process")
	timeRun := flag.Int("t", 60, "time of supervision")
	flag.Parse()

	// Prepare chans
	done := make(chan bool)
	results := make(chan HTTPResponse, *numJobs)

	// Prepare time of end of app
	endTime := time.Now().Local().Add(time.Second * time.Duration(*timeRun))

	// Create a go routine to stop the app at end of timer
	go func() {
		time.Sleep(time.Duration(*timeRun) * time.Second)
		done <- true
		close(results)
	}()

	// Create a go routine to store results and print stats
	go storeRes(results, done, *reqBySec, *numJobs, endTime)

	// Create 'numJobs' go routines to fetch url each delayed by t
	t := (time.Duration(1000000 / *reqBySec) * time.Microsecond)
	for w := 1; w <= *numJobs; w++ {
		time.Sleep(t)
		go get(*serverUrl,
			time.NewTicker(time.Duration(*numJobs)*t),
			done, results)
	}

	<-done
}
