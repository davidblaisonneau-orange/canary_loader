# Canary Loader

This program load web server and test canary release as described in
https://particule.io/blog/argocd-canary/. It shows proportion of server
answers, to illustrate canary deployment.

## Build

install [`term`](https://pkg.go.dev/golang.org/x/term) dependancy.
```
go get -u golang.org/x/term
```

```
go build canary_loader.go
```

## Run it

```
./canary_loader -url http://myservice_under_migration
```

```
Canary demo
===========
Requests by seconds (real / target): 1000 / 1000
Ends in: 199.767931ms
Status repartition: [ok]
Percent: ######################################################################
Colors repartition:  [blue red]
Percent: ######################################################_______________
```
## Usage

```
Usage of ./canary_loader:
  -p int
    	parallele process (default 5)
  -req int
    	requests by seconds (default 1000)
  -t int
    	time of supervision (default 60)
  -url string
    	API url (default "http://127.0.0.1:8090")
```

## Test it

An http_server is available to simulate a service under test. It answers a
different color depending on time minutes.

install [`fasthttp`](https://github.com/valyala/fasthttp) dependancy.
```
go get -u github.com/valyala/fasthttp
```

On one terminal: ```go run http_server.go```

On the other terminal: ```./canary_loader.go```