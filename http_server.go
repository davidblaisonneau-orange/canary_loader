package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"sync/atomic"
	"time"

	"github.com/valyala/fasthttp"
)

var readOps uint64

type HTTPResponse struct {
	Status string `json:"status"`
	Color  string `json:"color"`
}

func fastHTTPHandler(ctx *fasthttp.RequestCtx) {

	t, _ := strconv.Atoi(time.Now().Format("5"))
	var color string
	if rand.Intn(60) > t {
		color = "red"
	} else {
		color = "blue"
	}
	resp := HTTPResponse{
		Status: "ok",
		Color:  color}
	err := json.NewEncoder(ctx).Encode(resp)
	if err != nil {
		fmt.Fprintf(ctx, "Error")
		return
	}
	atomic.AddUint64(&readOps, 1)
}

func printReqBySec(
	ticker *time.Ticker) { // Ticker for status refresh
	fmt.Println("Demo HTTP Server.")
	for {
		select {
		case <-ticker.C:
			fmt.Fprintf(os.Stdout, "\rRequests by seconds: %9d", readOps)
			atomic.StoreUint64(&readOps, 0)
		}
	}
}

func main() {
	go printReqBySec(time.NewTicker(time.Duration(time.Second)))
	m := func(ctx *fasthttp.RequestCtx) {
		switch string(ctx.Path()) {
		case "/":
			fastHTTPHandler(ctx)
		default:
			ctx.Error("not found", fasthttp.StatusNotFound)
		}
	}

	fasthttp.ListenAndServe(":8090", m)
}
